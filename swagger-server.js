var swagger = require('swagger-framework');

var host = '127.0.0.1';
var port = 8000;
var url = 'http://' + host + ':' + port;

var framework = swagger.Framework({
  basePath: url
});

var api = framework.api({
  path: '/',
  description: 'Account Management',
  consumes: ['application/json'],
  produces: ['application/json'],
});

var resource = api.resource({
  path: '/signup'
});

var operation = resource.operation({
    method: 'POST',
    summary: 'Create a new account',
    notes: 'Returns the user created in json format',
    type: 'User',
    nickname: 'createNewUser',
    parameters: [{
      name: 'email',
      description: 'The new users email',
      required: true,
      type: 'string',
      paramType: 'path',
    }, {
      name: 'password',
      description: 'The new users password',
      required: true,
      type: 'string',
      paramType: 'path',
    }, ],
    responseMessages: [{
      code: 404,
      message: 'Failed to signup',
    }, ],
  },
  function (req, res) {
    res.sf.reply(200, {
      status: 'success',
      data: {
        _id: "53f5e86a1663a149abb68764",
        authentication_token: "$2a$10$BXswGstDoNz3PYySAe1jwe",
        password: "$2a$08$2dI9z/hm4bguUIYuD3yq5.0KmZ1APT53hGv0Rgs504aN8iyE3M4wO",
        email: "yassire20@ceremium.com",
      },
    });
  }
);

api.model({
  id: 'User',
  required: ['email', 'password'],
  properties: {
    email: {
      type: 'string',
      description: 'unique identifier for the user',
    },
    password: {
      type: 'string',
      description: 'the user password',
    },
    authentication_token: {
      type: 'string'
    },
  },
});

if (module.parent) {
  module.exports = framework;
} else {
  framework.server().listen(port, host, function (err) {
    if (err) throw err;

    console.log('Server started ' + url + '/');
  });
}
