// main.js
var User = require('../app/models/user');

var authenticate = function (token, response) {
    if (token == null) {
        response.jerror(new Error("Please provide authentication_token."));
    }
    User.findOne({
        'authentication_token': token
    }, function (err, user) {
        if (user == null) {
            response.jerror(new Error("Invalid authentication_token"));
        }
        else {
            return;
        }

    });
};

var authenticate_user = function (token, response, callback) {
    if (token == null) {
        response.jerror(new Error("Please provide authentication_token."));
    }
    User.findOne({
        'authentication_token': token
    }, function (err, user) {
        if (user == null) {
            response.jerror(new Error("Invalid authentication_token"));
        }
        else {
            return callback(user);
        }

    });
};

var authenticate_user_with_role = function (token, role, response, callback) {
    if (token == null) {
        response.jerror(new Error("Please provide authentication_token."));
    }
    User.findOne({
        'authentication_token': token,
        'role': role
    }, function (err, user) {
        if (user == null) {
            response.jerror(new Error("Invalid authentication_token"));
        }
        else {
            return callback(user);
        }

    });
};

exports.authenticate = authenticate;
exports.authenticate_user = authenticate_user;
exports.authenticate_user_with_role = authenticate_user_with_role;
