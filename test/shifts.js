var should = require('should');
var assert = require('assert');
var request = require('supertest');
var mongoose = require('mongoose');
var express = require('express');
var Staff = require('../app/models/shift');

describe('Routing', function () {
    var url = 'http://localhost:8080';
    var User = require('../app/models/user');
    var user = new User();
    var admin = new User();
    var Shift = require('../app/models/shift');
    var shift = new Shift();
    var Request = require('../app/models/request');
    var Trust = require('../app/models/trust');
    var Impression = require('../app/models/impression');
    var shiftRequest = new Request.Model();
    var impression = new Impression.Model();
    var trust = new Trust.Model();
    var today = new Date();
    var tomorrow = new Date();
    var yesterday = new Date();
    tomorrow.setDate(today.getDate() + 1);
    yesterday.setDate(today.getDate() - 1);

    var app = require('../server').app;
    // within before() you can run all the operations that are needed to setup your tests. In this case
    // I want to create a connection with the database, and when I'm done, I call done().
    before(function (done) {
        var mongodb = null;
        if (process.env.TEST_ENV == 'wercker') {
            console.log('Wercker configuration')
            var mongodb = mongoose.createConnection('mongodb://' + process.env.WERCKER_MONGODB_HOST + ':' + process.env.WERCKER_MONGODB_PORT + '/frameshift');
        }
        else {
            var mongodb = mongoose.connect('mongodb://127.0.0.1:27017/frameshift');
        }

        mongoose.connection.on('error', function (err) {
            console.log(err);
        });

        trust.name = 'Poplar Hospital';
        trust.latitude = 51.517358
        trust.longitude = -0.019114
        trust.save();

        user.email = 'locum.doctor@nhs.uk';
        user.password = user.generateHash('password');
        user.authentication_token = user.generateToken();
        user.grade = 'Level 1';
        user.speciality = 'Anaesthetics';
        user.save();

        admin.email = 'admin@ceremium.com';
        admin.password = admin.generateHash('password');
        admin.authentication_token = admin.generateToken();
        admin.grade = 'Level 1';
        admin.speciality = 'Anaesthetics';
        admin.role = "Trust Manager"
        admin.save();

        shift.name = 'Night Shift';
        shift.specialism = 'Anaesthetics';
        shift.grade = 'Level 1';
        shift.owner = admin;
        shift.trust = trust;
        shift.startDate = yesterday;
        shift.endDate = tomorrow;
        shift.save();

        shiftRequest.shift = shift;
        shiftRequest.locum = user;
        shiftRequest.save();

        done();
    });

    describe('shifts', function () {
        it('cannot create a shift if not authenticated', function (done) {
            var body = {
                name: 'Night Shift',
                startDate: '08-23-2014',
                endDate: '08-24-2014',
                address: 'Locksons close E14 6BH',
            };
            request(app)
                .post('/shift')
                .send(body)
                .expect(200) //Status code
            .end(function (err, res) {
                if (err) {
                    throw err;
                }
                res.body.should.have.property('status');
                res.body.should.have.property('message');
                message = res.body['message'];
                status = res.body['status'];
                message.should.equal('Please provide authentication_token.');
                status.should.equal('error');
                done();
            });
        });
        it('returns all shifts', function (done) {
            var data = {
                "authentication_token": user.authentication_token
            }
            request(app)
                .get('/shifts')
                .send(data)
                .expect(200) //Status code
            .end(function (err, res) {
                if (err) {
                    throw err;
                }
                res.body.should.have.property('status');
                res.body.should.have.property('data');
                status = res.body['status'];
                status.should.equal('success');
                done();
            });
        });
        it('searches for a shift by name', function (done) {
            var data = {
                "authentication_token": user.authentication_token,
                "name": "Night Shift"
            };
            request(app)
                .get('/shifts')
                .send(data)
                .expect(200) //Status code
            .end(function (err, res) {
                if (err) {
                    throw err;
                }
                res.body.should.have.property('status');
                res.body.should.have.property('data');
                data = res.body['data'][0];
                status = res.body['status'];
                status.should.equal('success');
                data['name'].should.equal('Night Shift');
                done();
            });
        });
    });
    it('searches for a shift by specialism and grade', function (done) {
        var data = {
            "authentication_token": user.authentication_token,
            "specialism": "Anaesthetics",
            "grade": "Level 1"
        };
        request(app)
            .get('/shifts')
            .send(data)
            .expect(200) //Status code
        .end(function (err, res) {
            if (err) {
                throw err;
            }

            res.body.should.have.property('status');
            res.body.should.have.property('data');

            responseData = res.body['data'][0];
            status = res.body['status'];

            status.should.equal('success');

            responseData['name'].should.equal('Night Shift');
            responseData['specialism'].should.equal('Anaesthetics');
            responseData['grade'].should.equal('Level 1');
            done();
        });
    });
    it('searches for a shift by a key word', function (done) {
        var data = {
            "authentication_token": user.authentication_token,
            "criteria": "night"
        };
        request(app)
            .get('/shifts')
            .send(data)
            .expect(200) //Status code
        .end(function (err, res) {
            if (err) {
                throw err;
            }
            res.body.should.have.property('status');
            res.body.should.have.property('data');
            responseData = res.body['data'][0];
            status = res.body['status'];
            status.should.equal('success');
            responseData['name'].should.equal('Night Shift');
            responseData['specialism'].should.equal('Anaesthetics');
            responseData['grade'].should.equal('Level 1');
            done();
        });
    });
    it('searches for a shift by a trust keyword', function (done) {
        var data = {
            "authentication_token": user.authentication_token,
            "criteria": "poplar"
        };
        request(app)
            .get('/shifts')
            .send(data)
            .expect(200) //Status code
        .end(function (err, res) {
            if (err) {
                throw err;
            }
            res.body.should.have.property('status');
            res.body.should.have.property('data');
            data = res.body['data'][0];
            status = res.body['status'];
            status.should.equal('success');
            data['name'].should.equal('Night Shift');
            data['specialism'].should.equal('Anaesthetics');
            data['grade'].should.equal('Level 1');
            done();
        });
    });
    it('searches for a shift by date', function (done) {
        var data = {
            "authentication_token": user.authentication_token,
            "date": today
        }
        request(app)
            .get('/shifts')
            .send(data)
            .expect(200) //Status code
        .end(function (err, res) {
            if (err) {
                throw err;
            }
            res.body.should.have.property('status');
            res.body.should.have.property('data');
            data = res.body['data'][0];
            status = res.body['status'];
            status.should.equal('success');
            data['name'].should.equal('Night Shift');
            data['specialism'].should.equal('Anaesthetics');
            data['grade'].should.equal('Level 1');
            done();
        });
    });
    it('searches for a shift by from and to date', function (done) {
        var data = {
            "authentication_token": user.authentication_token,
            "startDate": yesterday,
            "endDate": today
        };
        request(app)
            .get('/shifts')
            .send(data)
            .expect(200) //Status code
        .end(function (err, res) {
            if (err) {
                throw err;
            }
            res.body.should.have.property('status');
            res.body.should.have.property('data');
            data = res.body['data'][0];
            status = res.body['status'];
            status.should.equal('success');
            data['name'].should.equal('Night Shift');
            data['specialism'].should.equal('Anaesthetics');
            data['grade'].should.equal('Level 1');
            done();
        });
    });
    it('searches for a shift by postcode', function (done) {
        var data = {
            "authentication_token": user.authentication_token,
            "postcode": "E14 6BH"
        };
        request(app)
            .get('/shifts')
            .send(data)
            .expect(200) //Status code
        .end(function (err, res) {
            if (err) {
                throw err;
            }

            res.body.should.have.property('status');
            res.body.should.have.property('data');

            responseData = res.body['data'][0];

            status = res.body['status'];
            status.should.equal('success');
            responseData['name'].should.equal('Night Shift');
            responseData['specialism'].should.equal('Anaesthetics');
            responseData['grade'].should.equal('Level 1');
            done();
        });
    });
    it('should apply for a shift', function (done) {
        var data = {
            "authentication_token": user.authentication_token
        }
        request(app)
            .post('/shifts/' + shift.id + '/apply')
            .send(data)
            .expect(200) //Status code
        .end(function (err, res) {
            if (err) {
                throw err;
            }
            res.body.should.have.property('status');
            res.body.should.have.property('data');
            data = res.body['data'][0];
            status = res.body['status'];
            status.should.equal('success');
            done();
        });
    });
    it('cannot apply for invalid shift', function (done) {
        var data = {
            "authentication_token": user.authentication_token
        }
        request(app)
            .post('/shifts/invalid/apply')
            .send(data)
            .expect(200) //Status code
        .end(function (err, res) {
            if (err) {
                throw err;
            }
            res.body.should.have.property('status');
            res.body.should.have.property('message');
            message = res.body['message'];
            status = res.body['status'];
            message.should.equal('Shift not found.');
            status.should.equal('error');
            done();
        });
    });
    it('accepts a request', function (done) {
        var data = {
            "authentication_token": admin.authentication_token
        }
        request(app)
            .post('/requests/' + shiftRequest.id + '/approve')
            .send(data)
            .expect(200) //Status code
        .end(function (err, res) {
            if (err) {
                throw err;
            }
            res.body.should.have.property('status');
            res.body.should.have.property('data');
            status = res.body['status'];
            data = res.body['data'];
            status.should.equal('success');
            data.status.should.equal('Accepted');
            data.shift._id.should.equal(shift.id);
            data.locum.id.should.equal(user.id);
            done();
        });
    });
    it('cannot accept a request if not admin', function (done) {
        var data = {
            "authentication_token": user.authentication_token
        }
        request(app)
            .post('/requests/' + shiftRequest.id + '/approve')
            .send(data)
            .expect(200) //Status code
        .end(function (err, res) {
            if (err) {
                throw err;
            }
            res.body.should.have.property('status');
            res.body.should.have.property('message');
            status = res.body['status'];
            message = res.body['message'];
            status.should.equal('error');
            message.should.equal('Invalid authentication_token');
            done();
        });
    });
    it('rejects a request', function (done) {
        var data = {
            "authentication_token": admin.authentication_token
        }
        request(app)
            .post('/requests/' + shiftRequest.id + '/reject')
            .send(data)
            .expect(200) //Status code
        .end(function (err, res) {
            if (err) {
                throw err;
            }
            res.body.should.have.property('status');
            res.body.should.have.property('data');
            status = res.body['status'];
            data = res.body['data'];
            status.should.equal('success');
            data.status.should.equal('Rejected');
            data.shift._id.should.equal(shift.id);
            data.locum.id.should.equal(user.id);
            done();
        });
    });
    it('cannot accept a request if not admin', function (done) {
        var data = {
            "authentication_token": user.authentication_token
        }
        request(app)
            .post('/requests/' + shiftRequest.id + '/reject')
            .send(data)
            .expect(200) //Status code
        .end(function (err, res) {
            if (err) {
                throw err;
            }
            res.body.should.have.property('status');
            res.body.should.have.property('message');
            status = res.body['status'];
            message = res.body['message'];
            status.should.equal('error');
            message.should.equal('Invalid authentication_token');
            done();
        });
    });
    it('withdraw an application', function (done) {
        var data = {
            "authentication_token": user.authentication_token
        }
        shiftRequest.status = Request.pending;
        shiftRequest.save();
        request(app)
            .post('/shifts/' + shift.id + '/withdraw')
            .send(data)
            .expect(200) //Status code
        .end(function (err, res) {
            if (err) {
                throw err;
            }
            res.body.should.have.property('status');
            res.body.should.have.property('data');
            data = res.body['data'];
            message = res.body['message'];
            data['locum'].should.equal(user.id);
            data['shift'].should.equal(shift.id);
            data['status'].should.equal('Withdrawn');
            done();
        });
    });
    it('cannot withdraw an application if it doesnt belong to the user', function (done) {
        var data = {
            "authentication_token": user.authentication_token
        }
        shiftRequest.status = Request.pending;
        shiftRequest.save();
        request(app)
            .post('/shifts/' + shift.id + '/withdraw')
            .send(data)
            .expect(200) //Status code
        .end(function (err, res) {
            if (err) {
                throw err;
            }
            res.body.should.have.property('status');
            res.body.should.have.property('message');
            status = res.body['status'];
            message = res.body['message'];
            status.should.equal('error');
            message.should.equal('You have not applied for this shift or the request is not valid');
            done();
        });
    });
    it('cannot show the application if the user is not the shift owner', function (done) {
        var data = {
            "authentication_token": user.authentication_token
        }
        request(app)
            .get('/shifts/' + shift.id + '/applications')
            .send(data)
            .expect(200) //Status code
        .end(function (err, res) {
            if (err) {
                throw err;
            }
            res.body.should.have.property('status');
            res.body.should.have.property('message');
            status = res.body['status'];
            message = res.body['message'];
            status.should.equal('error');
            message.should.equal('Invalid authentication_token');
            done();
        });
    });
    it('returns all the applications for the shift owner', function (done) {
        var data = {
            "authentication_token": admin.authentication_token
        }
        request(app)
            .get('/shifts/' + shift.id + '/applications')
            .send(data)
            .expect(200) //Status code
        .end(function (err, res) {
            if (err) {
                throw err;
            }
            res.body.should.have.property('status');
            res.body.should.have.property('data');
            status = res.body['status'];
            responseData = res.body['data'];
            status.should.equal('success');
            responseData.length.should.equal(1);
            done();
        });
    });
    it('Adds a new impressions after search', function (done) {
        var data = {
            "authentication_token": user.authentication_token,
            "criteria": "night"
        };

        Impression.Model.count({}, function (err, initialCount) {
            request(app)
                .get('/shifts')
                .send(data)
                .expect(200) //Status code
            .end(function (err, res) {
                if (err) {
                    throw err;
                }
                res.body.should.have.property('status');
                res.body.should.have.property('data');
                responseData = res.body['data'][0];
                status = res.body['status'];
                status.should.equal('success');
                responseData['name'].should.equal('Night Shift');
                responseData['specialism'].should.equal('Anaesthetics');
                responseData['grade'].should.equal('Level 1');
                Impression.Model.count({}, function (err, finalCount) {
                    finalCount.should.equal(res.body['data'].length + initialCount);
                });
                done();
            });

        });
    });
    it('Adds a new impression after shift view', function (done) {
        var data = {
            "authentication_token": user.authentication_token
        };

        Impression.Model.count({}, function (err, initialCount) {
            request(app)
                .get('/shifts/' + shift.id)
                .send(data)
                .expect(200) //Status code
            .end(function (err, res) {
                if (err) {
                    throw err;
                }
                res.body.should.have.property('status');
                res.body.should.have.property('data');
                status = res.body['status'];
                status.should.equal('success');
                Impression.Model.count({}, function (err, finalCount) {
                    finalCount.should.equal(1 + initialCount);
                });
                done();
            });

        });
    });
    describe('users', function () {
        it('returns the current user profile', function (done) {
            var data = {
                "authentication_token": user.authentication_token
            }
            request(app)
                .get('/profile')
                .send(data)
                .expect(200) //Status code
            .end(function (err, res) {
                if (err) {
                    throw err;
                }
                res.body.should.have.property('status');
                res.body.should.have.property('data');
                status = res.body['status'];
                status.should.equal('success');
                done();
            });
        });
    });
});
