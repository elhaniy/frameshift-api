module.exports = function (grunt) {

  grunt.loadNpmTasks('grunt-mocha-test');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-jsbeautifier');

  // Project configuration.
  grunt.initConfig({
    mochaTest: {
      test: {
        options: {
          reporter: "spec",
          timeout: 10000
        },
        src: ["test/**/*.js"]
      }
    },
    jshint: {
      options: {
        jshintrc: '.jshintrc'
      },
      gruntfile: {
        src: 'Gruntfile.js'
      },
      lib: {
        src: ['**/*.js']
      }
    },
    watch: {
      gruntfile: {
        files: '<%= jshint.gruntfile.src %>',
        tasks: ['jshint:gruntfile']
      },
      lib: {
        files: '<%= jshint.lib.src %>',
        tasks: ['jshint:lib', 'jsbeautifier:check']
      }
    },
    jsbeautifier: {
      beautify: {
        src: ['Gruntfile.js', 'lib/*.js'],
        options: {
          config: '.jsbeautifyrc'
        }
      },
      check: {
        src: ['package.json', 'app/**/*.js', 'routes/**/*.js', 'config/**/*.js', 'modules/**/*.js', 'test/**/*.js'],
        options: {
          mode: 'VERIFY_AND_WRITE',
          config: '.jsbeautifyrc'
        }
      }
    }
  });

  // default task
  grunt.registerTask('default', ['jsbeautifier:check', 'mochaTest']);
  grunt.registerTask('beautify', ['jsbeautifier:beautify']);

};
