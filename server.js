// server.js

// set up ======================================================================
// get all the tools we need
var cluster = require('cluster');
var express = require('express');
var app = express();
var port = process.env.PORT || 80;
var mongoose = require('mongoose');
var passport = require('passport');
var numCPUs = require('os').cpus().length;

var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');

var configDB = require('./config/database.js');

var jsend = require('express-jsend');

console.log('Imported dependencies');

// export the app for supertest
exports.app = app;

// configuration ===============================================================
var env = process.env.NODE_ENV;

console.log('Running in ' + env)

if (env == 'development') {
  mongoose.connect('mongodb://localhost:27017/frameshift');
  mongoose.set('debug', true);
}

if (env == 'test') {
  mongoose.connect('mongodb://' + process.env.WERCKER_MONGODB_HOST + ':' + process.env.WERCKER_MONGODB_PORT + '/frameshift');
}

if (env == 'production') {
  mongoose.connect('mongodb://54.72.179.117:27017/frameshift');
}
require('./config/passport')(passport); // pass passport for configuration

// set up our express application
app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser.json()); // get information from html forms

app.set('view engine', 'ejs'); // set up ejs for templating

// required for passport
app.use(session({
  secret: 'frameshiftapi'
})); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(bodyParser.json());

// static files ================================================================= 
console.log('Setup static hosting...');
app.use(express.static(__dirname + '/public'));

// routes ======================================================================
require('./app/routes.js')(app, passport); // load our routes and pass in our app and fully configured passport
require('./app/routes/shifts.js')(app);
require('./app/routes/requests.js')(app);
require('./app/routes/trusts.js')(app);

// launch ======================================================================

if (cluster.isMaster && env != 'test') {
  // Fork workers.
  for (var i = 0; i < numCPUs; i++) {
    cluster.fork();
  }

  cluster.on('exit', function(worker, code, signal) {
    console.log('worker ' + worker.process.pid + ' died');
  });
} else {
  // Workers can share any TCP connection
  app.listen(port);
}


console.log('The server started on port ' + port);
