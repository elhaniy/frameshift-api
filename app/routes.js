// app/routes.js

// load up the models
var User = require('../app/models/user');
var Shift = require('../app/models/shift');
var bcrypt = require('bcrypt-nodejs');

// load modules
var main = require('../modules/main');

module.exports = function (app, passport) {

    // =====================================
    // HOME  ========
    // =====================================
    app.get('/', function (req, res) {
        res.jerror(new Error("No user logged in"));
    });

    // =====================================
    // ERRORS ==============================
    // =====================================
    app.get('/error_signup', function (req, res) {
        res.jerror(new Error("Failed to signup"));
    });

    app.get('/error_login', function (req, res) {
        res.jerror(new Error("Failed to login"));
    });

    // =====================================
    // PROFILE  =====================
    // =====================================
    app.get('/profile', function (req, res) {
        main.authenticate_user(req.body.authentication_token, res, function (current_user) {
            res.jsend(current_user.toJSON());
        });
    });

    // =====================================
    // LOGOUT ==============================
    // =====================================
    app.get('/logout', function (req, res) {
        req.logout();
        res.redirect('/');
    });

    // =====================================
    // SIGNUP ==============================
    // =====================================

    app.post('/signup', function (req, res) {
        User.register(new User({
            username: req.body.email,
            grade: req.body.grade,
            speciality: req.body.speciality,
            role: req.body.role,
            password: bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(8), null),
            authentication_token: bcrypt.genSaltSync(2)
        }), req.body.password, function (err, user) {
            if (err) {
                return res.jerror(err);
            }

            passport.authenticate('local')(req, res, function () {
                return res.jsend(user.toJSON());
            });
        });
    });

    // =====================================
    // LOGIN ===============================
    // =====================================

    app.post('/login', function (req, res) {
        User.findOne({
            'username': req.body.email
        }).populate('requests', 'ownedShifts').exec(function (err, user) {
            if (err)
                return res.jerror(err);

            if (!user)
                return res.jerror(new Error("No user found with email " + req.body.email));

            if (!user.validPassword(req.body.password)) {
                return res.jerror(new Error("Wrong password provided."));
            }

            return res.jsend(user.toJSON());
        });
    });

    // =====================================
    // DELETE ACCOUNT ======================
    // =====================================	
    app.delete('/account', function (req, res) {
        main.authenticate_user(req.body.authentication_token, res, function (current_user) {
            current_user.remove();
            res.jsend("Account deleted");
        });
    });

    // =====================================
    // CHANGE PASSWORD =====================
    // =====================================
    app.post('/password', function (req, res) {
        authentication_token = req.body.authentication_token;
        new_password = req.body.new_password;
        if (authentication_token == null) {
            res.jerror(new Error("Please provide authentication token."));
        }
        if (new_password == null) {
            res.jerror(new Error("Please provide the new password."));
        }
        User.findOne({
            'authentication_token': authentication_token
        }, function (err, user) {
            if (user == null) {
                res.jerror(new Error("Account not found"));
            }
            else {
                user.password = user.generateHash(new_password);
                user.authentication_token = user.generateToken();
                user.save();
                res.jsend(user);
            }

        });
    });

    // =====================================
    // RESET PASSWORD TOKEN=====================
    // =====================================
    app.post('/reset_password/token', function (req, res) {
        email = req.body.email;
        if (email == null) {
            res.jerror(new Error("Please provide an email address."));
        }
        User.findOne({
            'email': email
        }, function (err, user) {
            if (user == null) {
                res.jerror(new Error("Account not found"));
            }
            else {
                user.reset_password_token = user.generateToken();
                user.save();
                res.jsend(user.reset_password_token);
            }

        });
    });

    // =====================================
    // RESET PASSWORD =====================
    // =====================================
    app.post('/reset_password', function (req, res) {
        reset_password_token = req.body.reset_password_token;
        new_password = req.body.new_password;
        if (reset_password_token == null) {
            res.jerror(new Error("Please provide reset_password_token."));
        }
        if (new_password == null) {
            res.jerror(new Error("Please provide the new password."));
        }
        User.findOne({
            'reset_password_token': reset_password_token
        }, function (err, user) {
            if (user == null) {
                res.jerror(new Error("Account not found"));
            }
            else {
                user.password = user.generateHash(new_password);
                user.authentication_token = user.generateToken();
                user.reset_password_token = user.generateToken();
                user.save();
                res.jsend(user);
            }

        });
    });

};

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on 
    if (req.isAuthenticated())
        return next();

    res.redirect('/');
}
