// app/models/hospital.js
// load the things we need

var mongoose = require("mongoose"),
    Schema = mongoose.Schema,
    relationship = require("mongoose-relationship");

var hospitalSchema = new mongoose.Schema({
    name: String,
    shifts: [{
        type: Schema.ObjectId,
        ref: "Shift"
    }]
});

// create the model for hospitals and expose it to our app
module.exports = mongoose.model('Hospital', hospitalSchema);
