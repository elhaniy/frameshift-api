// app/models/trust.js
// load the things we need

var mongoose = require("mongoose"),
    Schema = mongoose.Schema,
    relationship = require("mongoose-relationship");

var trustSchema = new mongoose.Schema({
    name: String,
    latitude: Number,
    longitude: Number,
    shifts: [{
        type: Schema.ObjectId,
        ref: "Shift"
    }],
    manager: {
        type: Schema.ObjectId,
        ref: "User",
        childPath: "trusts"
    }
});

// create the model for trusts and expose it to our app
module.exports = {
    Model: mongoose.model('Trust', trustSchema)
};
