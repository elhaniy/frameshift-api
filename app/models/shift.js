// app/models/shift.js
// load the things we need
var mongoose = require('mongoose');

Schema = mongoose.Schema,
relationship = require("mongoose-relationship");

var GENERAL = "General";

var shiftSchema = new mongoose.Schema({
    name: String,
    startDate: Date,
    endDate: Date,
    rate: String,
    hospital: {
        type: Schema.ObjectId,
        ref: "Hospital",
        childPath: "shifts"
    },
    owner: {
        type: Schema.ObjectId,
        ref: "User",
        childPath: "ownedShifts"
    },
    specialism: {
        type: String,
        default: GENERAL
    },
    grade: String,
    address: String,
    description: String,
    requests: [{
        type: Schema.ObjectId,
        ref: "Request"
    }],
    trust: {
        type: Schema.ObjectId,
        ref: "Trust",
        childPath: "shifts"
    },
    hourly_rate: Number
});

shiftSchema.plugin(relationship, {
    relationshipPathName: 'hospital'
});
shiftSchema.plugin(relationship, {
    relationshipPathName: 'trust'
});
shiftSchema.plugin(relationship, {
    relationshipPathName: 'owner'
});

// create the model for shifts and expose it to our app
module.exports = mongoose.model('Shift', shiftSchema);
