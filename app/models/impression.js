// app/models/impression.js
// load the things we need
var mongoose = require('mongoose');

var SEARCH = "search_result";
var VIEW = "view_result";

Schema = mongoose.Schema,
relationship = require("mongoose-relationship");

var impressionSchema = new mongoose.Schema({
    type: {
        type: String
    },
    created_at: {
        type: Date,
        default: new Date()
    },
    shift: {
        type: Schema.ObjectId,
        ref: "Shift",
        childPath: "impressions"
    },
    user: {
        type: Schema.ObjectId,
        ref: "User",
        childPath: "impressions"
    }
});

impressionSchema.plugin(relationship, {
    relationshipPathName: 'shift'
});
impressionSchema.plugin(relationship, {
    relationshipPathName: 'user'
});

// create the model for impressions and expose it to our app
module.exports = {
    Model: mongoose.model('Impression', impressionSchema),
    search: SEARCH,
    view: VIEW
};
