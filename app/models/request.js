// app/models/request.js
// load the things we need
var mongoose = require('mongoose');

var PENDING = "Pending";
var ACCEPTED = "Accepted";
var REJECTED = "Rejected";
var WITHDRAWN = "Withdrawn";

Schema = mongoose.Schema,
relationship = require("mongoose-relationship");

var requestSchema = new mongoose.Schema({
    status: {
        type: String,
        default: PENDING
    },
    shift: {
        type: Schema.ObjectId,
        ref: "Shift",
        childPath: "requests"
    },
    locum: {
        type: Schema.ObjectId,
        ref: "User",
        childPath: "requests"
    }
});

requestSchema.plugin(relationship, {
    relationshipPathName: 'shift'
});
requestSchema.plugin(relationship, {
    relationshipPathName: 'locum'
});

// create the model for requests and expose it to our app
module.exports = {
    Model: mongoose.model('Request', requestSchema),
    pending: PENDING,
    accepted: ACCEPTED,
    rejected: REJECTED,
    withdrawn: WITHDRAWN
};
