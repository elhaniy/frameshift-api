// app/models/user.js
// load the things we need
var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
var passportLocalMongoose = require('passport-local-mongoose');

var LOCUM = "Locum";
var TRUST_MANAGER = "Trust Manager";
var ADMIN = "Admin";

// define the schema for our user model
var userSchema = mongoose.Schema({
    email: String,
    password: String,
    authentication_token: String,
    reset_password_token: String,
    grade: String,
    speciality: String,
    role: {
        type: String,
        default: LOCUM
    },
    requests: [{
        type: mongoose.Schema.ObjectId,
        ref: "Request"
    }],
    ownedShifts: [{
        type: mongoose.Schema.ObjectId,
        ref: "Shift"
    }]
});

// methods ======================
// generating a hash
userSchema.methods.generateHash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
};

// generate authentication token
userSchema.methods.generateToken = function () {
    return bcrypt.genSaltSync(2);
};

userSchema.methods.toJSON = function () {
    return {
        id: this.id,
        email: this.username,
        password: this.password,
        authentication_token: this.authentication_token,
        grade: this.grade,
        speciality: this.speciality,
        role: this.role,
        requests: this.requests,
        ownedShifts: this.ownedShifts
    };
};

userSchema.plugin(passportLocalMongoose);

// create the model for users and expose it to our app
module.exports = mongoose.model('User', userSchema);

/*
module.exports = {
    Model: mongoose.model('User', userSchema),
    locum: LOCUM,
    trust_manager: TRUST_MANAGER,
    admin: ADMIN
};
*/
