// app/routes/shifts.js

// load up the models
var Shift = require('../models/shift');
var Hospital = require('../models/hospital');
var Request = require('../models/request');
var Trust = require('../models/trust');
var Impression = require('../models/impression');
var geocoderProvider = 'google';
var httpAdapter = 'https';
var extra = {
    apiKey: 'AIzaSyAaA8bK-kBlfhndPJxVoD_TXtvdd-M5Mb0',
    formatter: null
};
var geocoder = require('node-geocoder').getGeocoder(geocoderProvider, httpAdapter, extra);
var geo = require('node-geo-distance');

// load modules
var main = require('../../modules/main');

module.exports = function (app) {

    // =====================================
    // SEARCH FOR SHIFTS ===================
    // =====================================
    app.get('/shifts', function (req, res) {
        main.authenticate_user(req.body.authentication_token, res, function (current_user) {
            var query = Shift.find({
                grade: current_user.grade
            });
            var criteria = req.body.criteria || 'XXXXXXXXXXXXXXXXXXXX';
            var shiftDate = req.body.date;
            var startDate = req.body.startDate;
            var endDate = req.body.endDate;
            var rate = req.body.rate;
            var postcode = req.body.postcode;

            Trust.Model.find({
                name: new RegExp(criteria, "i")
            }, function (err, trusts) {
                if (trusts != null && trusts.length > 0) {
                    for (var i = 0; i < trusts.length; i++) {
                        query.or([{
                            trust: trusts[i].id
                        }]);
                    }

                    query.or([{
                        name: new RegExp(criteria, "i")
                    }, {
                        description: new RegExp(criteria, "i")
                    }, {
                        specialism: new RegExp(criteria, "i")
                    }, {
                        grade: new RegExp(criteria, "i")
                    }]);

                }

                if (shiftDate != null) {
                    query.where('startDate').lt(new Date(shiftDate));
                    query.where('endDate').gt(new Date(shiftDate));
                }

                if (startDate != null && endDate != null) {
                    query.where('startDate').lt(new Date(startDate));
                    query.where('endDate').gt(new Date(endDate));
                }

                if (rate != null) {
                    query.where('hourly_rate').gt(rate);
                    console.log('rate == ' + rate);
                }

                if (current_user.speciality != null) {
                    query.where('specialism').in([current_user.speciality, 'General']);
                }
                else {
                    query.where('specialism').in(['General']);
                }

                if (postcode != null) {
                    return geocoder.geocode(postcode, function (err, data) {

                        if (!err && data != 'undefined') {

                            var location = data[0];
                            var coord1 = {
                                latitude: location.latitude,
                                longitude: location.longitude
                            }
                            Trust.Model.find(function (err, trusts) {
                                var i = 0;
                                var trustsArr = [];
                                while (trusts[i]) {
                                    if (trusts[i].latitude != null && trusts[i].longitude != null) {
                                        var coord2 = {
                                            latitude: trusts[i].latitude,
                                            longitude: trusts[i].longitude
                                        }

                                        geo.haversine(coord1, coord2, function (dist) {
                                            if (dist < 10) {
                                                trustsArr.push(trusts[i].id);
                                            }
                                        });
                                    }
                                    i++;
                                }
                                query.where('trust').in(trustsArr);
                                return query.populate('owner').populate('trust').exec(function (err, shifts) {
                                    if (!err) {
                                        var i = 0;
                                        while (shifts[i]) {
                                            var impression = new Impression.Model({
                                                shift: shifts[i],
                                                user: current_user,
                                                type: Impression.search
                                            });
                                            impression.save();
                                            i++;
                                        }
                                        return res.jsend(shifts);
                                    }
                                    else {
                                        res.jerror(new Error("Cannot retrieve shifts."));
                                    }
                                });
                            });

                        }
                        else {
                            res.jerror(new Error(err));
                        }
                    });
                }
                else {
                    return query.populate('owner').populate('trust').exec(function (err, shifts) {
                        if (!err) {
                            var i = 0;
                            while (shifts[i]) {
                                var impression = new Impression.Model({
                                    shift: shifts[i],
                                    user: current_user,
                                    type: Impression.search
                                });
                                impression.save();
                                i++;
                            }
                            return res.jsend(shifts);
                        }
                        else {
                            res.jerror(new Error("Cannot retrieve shifts."));
                        }
                    });
                }

            });
        });
    });

    // =====================================
    // CREATE A SHIFT ======================
    // =====================================
    app.post('/shift', function (req, res) {
        main.authenticate_user_with_role(req.body.authentication_token, 'Trust Manager', res, function (current_user) {
            var shift = new Shift({
                name: req.body.name,
                startDate: req.body.startDate,
                endDate: req.body.endDate,
                address: req.body.address,
                hospital: req.body.hospital_id,
                specialism: req.body.specialism,
                grade: req.body.grade,
                owner: current_user,
                trust: req.body.trust_id,
                description: req.body.description,
                hourly_rate: req.body.hourly_rate
            });
            shift.save(function (err) {
                if (!err) {
                    return res.jsend(shift);
                }
                else {
                    return res.jerror(err);
                }
            });
        });
    });

    // =====================================
    // CREATE A HOSPITAL ===================
    // =====================================
    app.post('/hospital', function (req, res) {
        main.authenticate_user_with_role(req.body.authentication_token, 'Trust Manager', res, function (current_user) {
            var hospital = new Hospital({
                name: req.body.name,
            });
            hospital.save(function (err) {
                if (!err) {
                    return res.jsend(hospital);
                }
                else {
                    return res.jerror(err);
                }
            });
        });
    });

    // =====================================
    // READ A SHIFT BY ID===================
    // =====================================
    app.get('/shifts/:id', function (req, res) {
        main.authenticate_user(req.body.authentication_token, res, function (current_user) {
            return Shift.findById(req.params.id).populate('owner').exec(function (err, shift) {
                if (!err && shift != null) {
                    var impression = new Impression.Model({
                        shift: shift,
                        user: current_user,
                        type: Impression.view
                    });
                    impression.save();
                    return res.jsend(shift);
                }
                else {
                    return res.jerror(new Error("Shift not found."));
                }
            });
        });
    });

    // =====================================
    // UPDATE A SHIFT BY ID=================
    // =====================================
    app.put('/shifts/:id', function (req, res) {
        main.authenticate_user_with_role(req.body.authentication_token, 'Trust Manager', res, function (current_user) {
            return Shift.findById(req.params.id).populate('owner').exec(function (err, shift) {
                shift.name = req.body.name;
                shift.startDate = req.body.startDate;
                shift.endDate = req.body.endDate;
                shift.address = req.body.address;
                return shift.save(function (err) {
                    if (!err) {
                        return res.jsend(shift);
                    }
                    else {
                        return res.jerror(new Error("Shift cannot be updated."));
                    }
                });
            });
        });
    });

    // =====================================
    // DELETE A SHIFT BY ID=================
    // =====================================
    app.delete('/shifts/:id', function (req, res) {
        main.authenticate_user_with_role(req.body.authentication_token, 'Trust Manager', res, function (current_user) {
            return Shift.findById(req.params.id, function (err, shift) {
                if (!err && shift != null) {
                    return shift.remove(function (err) {
                        return res.jsend("Removed");
                    });
                }
                else {
                    return res.jerror(new Error("Shift not found."));
                }
            });
        });
    });

    // =====================================
    // APPLY FOR A SHIFT ===================
    // =====================================
    app.post('/shifts/:id/apply', function (req, res) {
        main.authenticate_user_with_role(req.body.authentication_token, 'Locum', res, function (current_user) {
            return Shift.findById(req.params.id, function (err, shift) {
                if (!err && shift != null) {
                    var request = new Request.Model();
                    request.shift = shift;
                    request.locum = current_user;
                    request.save();
                    return res.jsend(request);
                }
                else {
                    return res.jerror(new Error("Shift not found."));
                }
            });
        });
    });

    // =====================================
    // Withdraw an application =============
    // =====================================
    app.post('/shifts/:id/withdraw', function (req, res) {
        main.authenticate_user_with_role(req.body.authentication_token, 'Locum', res, function (current_user) {
            return Shift.findById(req.params.id, function (err, shift) {
                if (!err && shift != null) {
                    return Request.Model.findOne({
                        shift: shift.id,
                        locum: current_user.id
                    }, function (err, request) {
                        if (!err && request != null && request.status == Request.pending) {
                            request.status = Request.withdrawn;
                            request.save();
                            return res.jsend(request);
                        }
                        else {
                            return res.jerror(new Error("You have not applied for this shift or the request is not valid"));
                        }
                    });
                }
                else {
                    return res.jerror(new Error("Shift not found."));
                }
            });
        });
    });

    // ==========================================
    // GET all valid applications for a shift ===
    // ==========================================
    app.get('/shifts/:id/applications', function (req, res) {
        main.authenticate_user_with_role(req.body.authentication_token, 'Trust Manager', res, function (current_user) {
            return Shift.findById(req.params.id, function (err, shift) {
                if (!err && shift != null && shift.owner == current_user.id) {
                    var query = Request.Model.find({
                        shift: shift.id
                    });
                    query.where('status').in([Request.pending, Request.accepted]);
                    return query.populate('locum').populate('shift').exec(function (err, requests) {
                        if (!err && requests != null) {
                            return res.jsend(requests);
                        }
                        else {
                            return res.jerror(new Error("No requests found in pending or accepted state."));
                        }
                    });
                }
                else {
                    return res.jerror(new Error("Shift not found or you are not the owner."));
                }
            });
        });
    });

};
