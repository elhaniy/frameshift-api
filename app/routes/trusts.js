// app/routes/trusts.js

// load up the models
var Trust = require('../models/trust');

// load modules
var main = require('../../modules/main');

module.exports = function (app) {

    app.post('/trusts', function (req, res) {
        main.authenticate_user_with_role(req.body.authentication_token, 'Trust Manager', res, function (current_user) {
            var trust = new Trust.Model({
                name: req.body.name,
                latitude: req.body.latitude,
                longitude: req.body.longitude,
                manager: current_user
            });
            trust.save(function (err) {
                if (!err) {
                    return res.jsend(trust);
                }
                else {
                    return res.jerror(err);
                }
            });
        });
    });
}
