// app/routes/requests.js

// load up the models
var Shift = require('../models/shift');
var Request = require('../models/request');

// load modules
var main = require('../../modules/main');

module.exports = function (app) {
    app.post('/requests/:id/approve', function (req, res) {
        main.authenticate_user_with_role(req.body.authentication_token, 'Trust Manager', res, function (current_user) {
            return Request.Model.findById(req.params.id).populate('locum').populate('shift').exec(function (err, request) {
                if (!err && request != null) {
                    return Shift.findById(request.shift, function (err, shift) {
                        if (!err && shift != null && shift.owner == current_user.id) {
                            request.status = Request.accepted;
                            request.save();
                            return res.jsend(request);
                        }
                        else {
                            return res.jerror(new Error("Only the shift owner can approve this request."));
                        }
                    });
                }
                else {
                    return res.jerror(new Error("Request not found."));
                }
            });
        });
    });

    app.post('/requests/:id/reject', function (req, res) {
        main.authenticate_user_with_role(req.body.authentication_token, 'Trust Manager', res, function (current_user) {
            return Request.Model.findById(req.params.id).populate('locum').populate('shift').exec(function (err, request) {
                if (!err && request != null) {
                    return Shift.findById(request.shift, function (err, shift) {
                        if (!err && shift != null && shift.owner == current_user.id) {
                            request.status = Request.rejected;
                            request.save();
                            return res.jsend(request);
                        }
                        else {
                            return res.jerror(new Error("Only the shift owner can reject this request."));
                        }
                    });
                }
                else {
                    return res.jerror(new Error("Request not found."));
                }
            });
        });
    });
};
