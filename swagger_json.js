{
  "apiVersion": "1.0.0",
  "swaggerVersion": "1.2",
  "basePath": "http://127.0.0.1:8000/api",
  "resourcePath": "/user",
  "produces": [
    "application/json"
  ],
  "apis": [{
    "path": "/signup",
    "operations": [{
      "method": "POST",
      "summary": "Create a new locum",
      "notes": "Returns the new created locum in json",
      "type": "User",
      "parameters": [{
        "email": "email",
        "description": "The new user email",
        "required": true,
        "type": "string",
        "allowMultiple": false
      }, {
        "password": "password",
        "description": "The new user password",
        "required": true,
        "type": "string",
        "allowMultiple": false
      }],
      "responseMessages": [{
        "code": 400,
        "message": "unable to signup"
      }]
    }]
  }, {
    "path": "/login",
    "operations": [{
      "method": "POST",
      "summary": "Authenticate a locum",
      "notes": "Returns the authenticated locum",
      "type": "User",
      "parameters": [{
        "email": "email",
        "description": "The user email",
        "required": true,
        "type": "string",
        "allowMultiple": false
      }, {
        "password": "password",
        "description": "The user password",
        "required": true,
        "type": "string",
        "allowMultiple": false
      }],
      "responseMessages": [{
        "code": 400,
        "message": "unable to login"
      }]
    }]
  }, {
    "path": "/account",
    "operations": [{
      "method": "DELETE",
      "summary": "Deletes an account",
      "notes": "Returns a message indicating that the account was deleted",
      "type": "User",
      "parameters": [{
        "authentication_token": "authentication_token",
        "description": "The user authentication_token",
        "required": true,
        "type": "string",
        "allowMultiple": false
      }],
      "responseMessages": [{
        "code": 400,
        "message": "unable to delete the account"
      }]
    }]
  }, {
    "path": "/password",
    "operations": [{
      "method": "POST",
      "summary": "Change the password",
      "notes": "Returns the user in json",
      "type": "User",
      "parameters": [{
        "authentication_token": "authentication_token",
        "description": "The user authentication_token",
        "required": true,
        "type": "string",
        "allowMultiple": false
      }, {
        "new_password": "new_password",
        "description": "The user new password",
        "required": true,
        "type": "string",
        "allowMultiple": false
      }],
      "responseMessages": [{
        "code": 400,
        "message": "unable to change thee password"
      }]
    }]
  }, {
    "path": "/reset_password/token",
    "operations": [{
      "method": "POST",
      "summary": "Returns the reset password token to reset the password",
      "notes": "Returns the reset password token",
      "type": "String",
      "parameters": [{
        "email": "email",
        "description": "The user email",
        "required": true,
        "type": "string",
        "allowMultiple": false
      }],
      "responseMessages": [{
        "code": 400,
        "message": "account not found"
      }]
    }]
  }, {
    "path": "/reset_password",
    "operations": [{
      "method": "POST",
      "summary": "Returns the user with the new password",
      "notes": "Returns the user json",
      "type": "String",
      "parameters": [{
        "reset_password_token": "reset_password_token",
        "description": "The user reset_password_token",
        "required": true,
        "type": "string",
        "allowMultiple": false
      }],
      "responseMessages": [{
        "code": 400,
        "message": "account not found"
      }]
    }]
  }, {
    "path": "/logout",
    "operations": [{
      "method": "GET",
      "summary": "Logout the user",
      "notes": "Returns the user json",
      "type": "String",
      "parameters": [{
        "authentication_token": "authentication_token",
        "description": "The user authentication_token",
        "required": true,
        "type": "string",
        "allowMultiple": false
      }],
      "responseMessages": [{
        "code": 400,
        "message": "account cannot be logged out"
      }]
    }]
  }],
  "models": {
    "User": {
      "id": "User",
      "properties": {
        "id": {
          "type": "integer",
          "format": "int64"
        },
        "email": {
          "type": "string"
        },
        "password": {
          "type": "string"
        },
        "authentication_token": {
          "type": "string"
        },
        "reset_password_token": {
          "type": "string"
        },
        "requests": {
          "type": "array"
        },
        "ownedShifts": {
          "type": "array"
        }
      }
    }
  }
}
